package com.zuitt.wdc044.models;
import javax.persistence.*;
//mark this Java object as a representation of a database table via @Entity
@Entity
//designate the table name
@Table(name = "users")

public class User {
    @Id
    //auto-increment
    @GeneratedValue
    private Long id;

    //class properties that represent a table column
    @Column
    private String username;

    @Column
    private String password;

    //default constructor, this is needed when retrieving posts
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //getter and setter
    public String getUserName(){
        return username;
    }

    public void setUserName(String title){
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String content){
        this.password = password;
    }
}
